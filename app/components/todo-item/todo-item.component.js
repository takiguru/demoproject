import { Component, EventEmitter, Output, Input } from '@angular/core';

import template from './todo-item.template.html';

@Component({
	selector: 'todo-item',
	template: template
})
export class TodoItemComponent {
	@Input() todo;

	@Output() itemModified = new EventEmitter();

	@Output() itemRemoved = new EventEmitter();

	@Output() addSubTask = new EventEmitter();

	editing = false;

    editingSub = false;

    editingSubIndex = -1;

    subtaskbox = false;

	subTaskInput = '';


	cancelEditing() {
		this.editing = false;
	}

    cancelEditingSub() {
        this.editingSub = false;
    }

	stopEditing(editedTitle) {
		this.todo.setTitle(editedTitle.value);
		this.editing = false;

		if (this.todo.title.length === 0) {
			this.remove();
		} else {
			this.update();
		}
	}

    stopEditingSub(index,editedTitleSub) {
		console.log('index:',index,'title:',editedTitleSub)
        this.todo.setSubTitle(index,editedTitleSub.value);
        this.editingSub = false;
        this.editingSubIndex = -1;
        if (this.todo.subtask[index].title.length === 0) {
            this.remove();
        } else {
            this.update();
        }
    }

	edit() {
		this.editing = true;
	}

    editSub(todo,index) {
        this.editingSub = true;
        console.log('index:',index)
        this.editingSubIndex = index
    }

    checkComplete(){
		let complete = true;
		this.todo.subtask.forEach(function (task) {
			if(task.completed === false){
				complete = false;
			}
        })
		if(complete){
            this.todo.subtask.forEach(function (task) {
                task.completed=false;
            })
			this.toggleCompletion()
		}else{
			this.todo.completed = false;
		}

	}

	toggleCompletion() {
    	let _this = this
		this.todo.completed = !this.todo.completed;
        this.todo.subtask.forEach(function (task) {
        	if(_this.todo.completed){
                task.completed = true
			}else{
                task.completed = !task.completed;
			}

        });
		this.update();
	}

    toggleCompletionSub(index) {
        this.todo.subtask[index].completed = !this.todo.subtask[index].completed;
        this.checkComplete();
        this.update();
    }

	remove() {
		this.itemRemoved.next(this.todo.uid);
	}

	removeSub(index){
    	let subtasks = this.todo.subtask
		delete subtasks.splice(index,1)
		this.checkComplete()
	}

	update() {
		this.itemModified.next(this.todo.uid)
	}

	subTaskBoxManager(){
		this.subtaskbox = !this.subtaskbox;
	}

	addsub(){
		console.log(this.todo.subtask)
		this.todo.subtask.push({title:this.subTaskInput,completed:false})
		this.update()
	}
}
